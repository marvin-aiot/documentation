# Marvin - Artificial Intelligence of Things

## Goals
*Marvin AIoT* is a concept for robust distributed automation based on theories of swarm intelligence and on Marvin Minsky’s Society of Mind (hence its name as a homage).
The devices in a Marvin network are independent of a central control instance (aka. hub or cloud) and implement (more or less) simple logic. As such, the behavior of the devices mimics the reflexes innate to the peripheral nervous system of animals and humans.

This devices range from extremely simple ESP8266 to high performing ODROIDs and even full sized and powerful computers.
The *Marvin AIoT* software exists in two main flavors:
- **Embedded Device**: This are software components specific to each microcontroller (ESPs, Arduinos, STMs, PICs etc.). They are developed very close to the hardware and allow interfacing many sensors.
- **Marvin Device**: This devices run the Marvin AIoT software itself, usually on better performing hardware (when compared to the *Embedded Devices*), such as Rapsberrys and ODROIDs. This hardware enables development of more complex behaviors and interfacing to more elaborate sensors (e.g. image and audio processing) and even functioning as gateways to other technologies (KNX, ZigBee and Lutron, among others).
A *Marvin Device* might also act as a Network Master, implementing higher level behaviors by listening to the other Devices and sending specific commands for the Devices to act upon.


## Possible applications
- Home/office/retail automation system
- Assisted living
- Face detection & recognition
- Voice recognition
- Incident detection (fire, smoke, accident, person unconscious...)
- Heatmap creation for retail analysis


## Installation

To install an **Embedded Device**, choose from [existing Devices](https://gitlab.com/marvin-aiot/embedded-device/devices) and open the associated .ino file with the Arduino IDE and flash the Device normaly.
  
To install a **Marvin Device** download and execute [the installer](https://gitlab.com/marvin-aiot/marvin-device/tools/marvin-installer/blob/99ba906065d22556b676a0034665ba6eda338245/scripts/install-marvin.sh).


## License Information

This library is released under the GNU General Public License. Please refer to the [LICENSE](https://gitlab.com/marvin-aiot/marvin-device/base/blob/master/LICENSE "License") to get the full licensing text.


## Contributing to this project

Please refer to the [CONTRIBUTING](https://gitlab.com/marvin-aiot/marvin-device/base/blob/master/CONTRIBUTING.md "Contributing") document for more information.

